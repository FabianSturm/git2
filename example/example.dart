// SPDX-License-Identifier: BSD-3-Clause

import 'package:git2/git2.dart';

void main() {
  final repo = GitRepository.open('.');
  final workdir = repo.workdir();
  print('The repository working directoy is: $workdir');
}
