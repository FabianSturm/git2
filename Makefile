SHELL = /bin/bash


install:
	pub get

lint:
	dartanalyzer --fatal-warnings --fatal-lints --fatal-hints .

test:
	# These are the dart unit tests
	pub run test


.PHONY: install lint test
