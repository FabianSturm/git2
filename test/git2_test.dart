// SPDX-License-Identifier: BSD-3-Clause

import 'package:git2/git2.dart';
import 'package:test/test.dart';

void main() {
  test('open non existing path', () {
    expect(
        () => GitRepository.open('doesnotexist'),
        throwsA(predicate((e) =>
            e is GitException &&
            e.error == GitErrorCode.GIT_ENOTFOUND &&
            e.klass == GitError.GITERR_OS)));
  });

  test('open existing git repo', () {
    final git = GitRepository.open('test_data');
    final workdir = git.workdir();
    expect(workdir, endsWith('/test_data/'));
  });

  test('list all branches and check that master branch exists', () {
    final git = GitRepository.open('test_data');
    final iterator = git.gitBranchIteratorNew(GitBranchT.GIT_BRANCH_ALL);
    var name = '';
    var ref = iterator.next();
    while (ref != null && name != 'master') {
      name = ref.gitBranchName();
      ref = iterator.next();
    }
    expect(name, 'master');
  });
}
