// SPDX-License-Identifier: BSD-3-Clause

part of 'git_repository.dart';

class GitException implements Exception {
  GitException(this.error, this.message, this.klass);

  factory GitException._fromError(int error) {
    // We don't need to free the pointer returned by sqlite3_errmsg: "Memory to
    // hold the error message string is managed internally. The application does
    // not need to worry about freeing the result."
    // https://www.sqlite.org/c3ref/errcode.html
    final gitError = bindings.giterr_last();

    if (gitError != nullptr) {
      return GitException(
          error, Utf8.fromUtf8(gitError.ref.message), gitError.ref.klass);
    } else {
      return GitException(error, 'No error occured', GitError.GITERR_NONE);
    }
  }

  final int error;
  final String message;
  final int klass;

  @override
  String toString() => 'Git2Exception: $error/$klass: $message';
}
