// SPDX-License-Identifier: BSD-3-Clause

import 'dart:ffi';

import 'package:ffi/ffi.dart';

import '../bindings/bindings.dart';
import '../bindings/types.dart' as types;

part 'constants.dart';
part 'errors.dart';

/// A libgit2 wrapper.
class GitRepository {
  GitRepository._(this._repo);

  factory GitRepository.open(String path) {
    var res = bindings.git_libgit2_init();
    if (res < 1) {
      throw Exception('libgit2 initialization failed');
    }

    final repository = allocate<Pointer<types.GitRepository>>();
    res = bindings.git_repository_open(repository, Utf8.toUtf8(path));

    if (res == 0) {
      final _repository = repository.value;
      free(repository);
      return GitRepository._(_repository);
    } else {
      free(repository);
      // FIXME: bindings.git_libgit2_shutdown();
      throw GitException._fromError(res);
    }
  }

  final Pointer<types.GitRepository> _repo;

  GitBranchIterator gitBranchIteratorNew(int listFlags) {
    final iterator = allocate<Pointer<types.GitBranchIterator>>();
    final res = bindings.git_branch_iterator_new(iterator, _repo, listFlags);

    if (res == 0) {
      final _iterator = iterator.value;
      free(iterator);
      return GitBranchIterator(_iterator);
    } else {
      free(iterator);
      throw GitException._fromError(res);
    }
  }

  String workdir() {
    final workdir = Utf8.fromUtf8(bindings.git_repository_workdir(_repo));
    return workdir;
  }

  // FIXME: add git_repository_free call
}

class GitBranchIterator {
  GitBranchIterator(this._iterator);

  final Pointer<types.GitBranchIterator> _iterator;

  GitReference next() {
    final reference = allocate<Pointer<types.GitReference>>();
    final flags = allocate<Int32>();

    final res = bindings.git_branch_next(reference, flags, _iterator);
    /*
    final _flags = flags.value;
    free(flags);
    // FIXME: return flags too
    */

    if (res == 0) {
      final _reference = reference.value;
      free(reference);
      return GitReference(_reference);
    } else {
      free(reference);
      return null;
    }
  }

// FIXME: add git_branch_iterator_free call
}

class GitReference {
  GitReference(this._reference);

  final Pointer<types.GitReference> _reference;

  String gitBranchName() {
    final name = allocate<Pointer<Utf8>>();
    final res = bindings.git_branch_name(name, _reference);

    if (res == 0) {
      final _name = name.value;
      free(name);
      return Utf8.fromUtf8(_name);
    } else {
      free(name);
      throw GitException._fromError(res);
    }
  }

// FIXME: add git_reference_free call
}
