// SPDX-License-Identifier: BSD-3-Clause

import 'dart:ffi';

import 'package:ffi/ffi.dart';

class GitError extends Struct {
  Pointer<Utf8> message;
  @Int32()
  int klass;
}

class GitRepository extends Struct {}

class GitBranchIterator extends Struct {}

class GitReference extends Struct {}
