// SPDX-License-Identifier: BSD-3-Clause
// ignore_for_file: non_constant_identifier_names

import 'dart:ffi';
import 'dart:io' show Platform;

import 'signatures.dart';

class _Git2Bindings {
  _Git2Bindings() {
    // Open the dynamic library
    var path = '/usr/lib/x86_64-linux-gnu/libgit2.so.27';
    if (Platform.isMacOS) {
      path = './hello_library/libhello.dylib';
    }
    if (Platform.isWindows) {
      path = 'hello_library\\libhello.dll';
    }
    _dylib = DynamicLibrary.open(path);

    // annotated
    // attr
    // blame
    // blob

    // branch
    git_branch_name = _dylib
        .lookup<NativeFunction<git_branch_name_native_t>>('git_branch_name')
        .asFunction<git_branch_name_t>();

    git_branch_iterator_new = _dylib
        .lookup<NativeFunction<git_branch_iterator_new_native_t>>(
            'git_branch_iterator_new')
        .asFunction<git_branch_iterator_new_t>();

    git_branch_next = _dylib
        .lookup<NativeFunction<git_branch_next_native_t>>('git_branch_next')
        .asFunction<git_branch_next_t>();

    // buf
    // checkout
    // cherrypick
    // clone
    // commit
    // config
    // cred
    // describe
    // diff
    // fetch
    // filter

    // giterr
    giterr_last = _dylib
        .lookup<NativeFunction<giterr_last_native_t>>('giterr_last')
        .asFunction<giterr_last_t>();

    // graph
    // ignore
    // index
    // indexer

    // libgit2
    git_libgit2_init = _dylib
        .lookup<NativeFunction<git_libgit2_init_native_t>>('git_libgit2_init')
        .asFunction<git_libgit2_init_t>();

    // merge
    // message
    // note
    // object
    // odb
    // oid
    // oidarray
    // packbuilder
    // patch
    // pathspec
    // proxy
    // push
    // rebase
    // refdb
    // reference
    // reflog
    // refspec
    // remote

    // repository
    git_repository_open = _dylib
        .lookup<NativeFunction<git_repository_open_native_t>>(
            'git_repository_open')
        .asFunction<git_repository_open_t>();

    git_repository_workdir = _dylib
        .lookup<NativeFunction<git_repository_workdir_native_t>>(
            'git_repository_workdir')
        .asFunction<git_repository_workdir_t>();

    // reset
    // revert
    // revparse
    // revwalk
    // signature
    // stash
    // status
    // strarray
    // submodule
    // tag
    // trace
    // tree
    // treebuilder
    // worktree
  }

  DynamicLibrary _dylib;

  // annotated
  // attr
  // blame
  // blob

  // branch
  git_branch_name_t git_branch_name;
  git_branch_iterator_new_t git_branch_iterator_new;
  git_branch_next_t git_branch_next;

  // buf
  // checkout
  // cherrypick
  // clone
  // commit
  // config
  // cred
  // describe
  // diff
  // fetch
  // filter

  // giterr
  giterr_last_t giterr_last;

  // graph
  // ignore
  // index
  // indexer

  // libgit2
  git_libgit2_init_t git_libgit2_init;

  // merge
  // message
  // note
  // object
  // odb
  // oid
  // oidarray
  // packbuilder
  // patch
  // pathspec
  // proxy
  // push
  // rebase
  // refdb
  // reference
  // reflog
  // refspec
  // remote

  // repository
  git_repository_open_t git_repository_open;
  git_repository_workdir_t git_repository_workdir;

  // reset
  // revert
  // revparse
  // revwalk
  // signature
  // stash
  // status
  // strarray
  // submodule
  // tag
  // trace
  // tree
  // treebuilder
  // worktree
}

_Git2Bindings _cachedBindings;
_Git2Bindings get bindings => _cachedBindings ??= _Git2Bindings();
