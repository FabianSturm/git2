// SPDX-License-Identifier: BSD-3-Clause
// ignore_for_file: non_constant_identifier_names

import 'dart:ffi';

import 'package:ffi/ffi.dart';

import 'types.dart';

// annotated
// attr
// blame
// blob

// branch
typedef git_branch_name_native_t = Int32 Function(
    Pointer<Pointer<Utf8>> out, Pointer<GitReference> ref);
typedef git_branch_name_t = int Function(
    Pointer<Pointer<Utf8>> out, Pointer<GitReference> ref);

typedef git_branch_iterator_new_native_t = Int32 Function(
    Pointer<Pointer<GitBranchIterator>> out,
    Pointer<GitRepository> repo,
    Int32 list_flags);
typedef git_branch_iterator_new_t = int Function(
    Pointer<Pointer<GitBranchIterator>> out,
    Pointer<GitRepository> repo,
    int list_flags);

typedef git_branch_next_native_t = Int32 Function(
    Pointer<Pointer<GitReference>> out,
    Pointer<Int32> out_type,
    Pointer<GitBranchIterator> iter);
typedef git_branch_next_t = int Function(Pointer<Pointer<GitReference>> out,
    Pointer<Int32> out_type, Pointer<GitBranchIterator> iter);

// buf
// checkout
// cherrypick
// clone
// commit
// config
// cred
// describe
// diff
// fetch
// filter

// giterr
typedef giterr_last_native_t = Pointer<GitError> Function();
typedef giterr_last_t = Pointer<GitError> Function();

// graph
// ignore
// index
// indexer

// libgit2
typedef git_libgit2_init_native_t = Int32 Function();
typedef git_libgit2_init_t = int Function();

// merge
// message
// note
// object
// odb
// oid
// oidarray
// packbuilder
// patch
// pathspec
// proxy
// push
// rebase
// refdb
// reference
// reflog
// refspec
// remote

// repository
typedef git_repository_open_native_t = Int32 Function(
    Pointer<Pointer<GitRepository>> out, Pointer<Utf8> path);
typedef git_repository_open_t = int Function(
    Pointer<Pointer<GitRepository>> out, Pointer<Utf8> path);

typedef git_repository_workdir_native_t = Pointer<Utf8> Function(
    Pointer<GitRepository>);
typedef git_repository_workdir_t = Pointer<Utf8> Function(
    Pointer<GitRepository>);

// reset
// revert
// revparse
// revwalk
// signature
// stash
// status
// strarray
// submodule
// tag
// trace
// tree
// treebuilder
// worktree
