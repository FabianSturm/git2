# git2

A libgit2 wrapper.

[![pipeline status](https://gitlab.com/FabianSturm/git2/badges/master/pipeline.svg)](https://gitlab.com/FabianSturm/git2/pipelines/latest)
[![coverage report](https://gitlab.com/FabianSturm/git2/badges/master/coverage.svg)](https://gitlab.com/FabianSturm/git2/-/jobs/artifacts/master/file/coverage/index.html?job=test)

This is an initial work on a libgit2 wrapper with dart:ffi which so far only implements a few methods to figure out how to setup a dart:ffi package.
Therefore it does not have any API stability guarantees. Comments, tips and merge request are welcome!


# license

This libgit2 wrapper is licensed under the 3 clause BSD license. But beware of the differing license of the wrapped libgit2 library.


# CI/CD

* Continous integration ([gitlab-runner](https://docs.gitlab.com/runner/)) [https://gitlab.com/FabianSturm/git2/pipelines/latest](https://gitlab.com/FabianSturm/git2/pipelines/latest)
* Test coverage ([test_coverage](https://pub.dev/packages/test_coverage)) [https://gitlab.com/FabianSturm/git2/-/jobs/artifacts/master/file/coverage/index.html?job=test](https://gitlab.com/FabianSturm/git2/-/jobs/artifacts/master/file/coverage/index.html?job=test)
